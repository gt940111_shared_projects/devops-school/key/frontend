import os
import requests
import re


# calculate bugfix version - checks special words in commit
fixIsChanged = False
fix = int(os.environ.get("DEVSCHOOL_V_FIX"))
if bool(re.search("(^fix|/s+fix/s+|bugfix)", os.environ.get("CI_COMMIT_MESSAGE").lower())):
    fix += 1
    fixIsChanged = True
    print("elevating fix version:", fix)


# calculate minor version - when pushing in 'dev' or with special wirds in commit
minorIsChanged = False
minor = int(os.environ.get("DEVSCHOOL_V_MINOR"))
if os.environ.get("CI_COMMIT_BRANCH") == 'dev' or bool(re.search("(push|docker|image|test)", os.environ.get("CI_COMMIT_MESSAGE").lower())):
    minor += 1
    minorIsChanged = True
    print("elevating minor version:", minor)


# calculate major version - only big changes
majorIsChanged = False
major = int(os.environ.get("DEVSCHOOL_V_MAJOR"))
if os.environ.get("CI_COMMIT_BRANCH") == 'master':
    major += 1
    majorIsChanged = True
    minor = 0
    minorIsChanged = True
    fix = 0
    fixIsChanged = True
    print("elevating major version:", major)


version = f"{major}.{minor}.{fix}"
print("current version is", version)



# pre-created params for requests to update variables:
headers = {'PRIVATE-TOKEN':os.environ.get("LEMME_REWRITE_IT")}
base_url = f'https://gitlab.com/api/v4/projects/{os.environ.get("CI_PROJECT_ID")}/variables/'

if majorIsChanged: requests.put(base_url + f'DEVSCHOOL_V_MAJOR?value={major}', headers=headers)
if minorIsChanged: requests.put(base_url + f'DEVSCHOOL_V_MINOR?value={minor}', headers=headers)
if fixIsChanged:   requests.put(base_url + f'DEVSCHOOL_V_FIX?value={fix}', headers=headers)

requests.put(f'https://gitlab.com/api/v4/groups/{os.environ.get("GROUP_ID")}/variables/FRONTEND_LATEST?value={version}', headers=headers)
print("End of script")
