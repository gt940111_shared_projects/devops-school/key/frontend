FROM openjdk:8-alpine

COPY devschool-front-app-server/build/libs/*.jar devschool-front-app-server.jar

EXPOSE 8081

ENTRYPOINT ["java", "-jar", "devschool-front-app-server.jar"]